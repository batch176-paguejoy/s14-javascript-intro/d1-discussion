
console.log('Hello World from external JS file!');

// Writing comments
	// single line ( ctrl / )
	/* multi-line ( ctrl shift / )  */


/* Syntax and Statements 
	
	Syntax= set of rules how codes should be written correctly


	Statement= set of instructions, ends with semicolon
*/


/* Variables 
		variable - a container that holds value/data
		value - a data that you assign to a variable

*/
	// let car = "rolls royce";
	let firstName = "Joy";
	let lastName = "Pague" ;
	let Batch = "Batch 176";

	let fullName = "Hi, my name is" + " " + firstName + " " + lastName + "." + " " + " I am from" + " " + Batch

	// ES6 update
		// template literals - backticks and ${}

	let fullName2 = `Hi, my name is ${firstName} ${lastName}. I am ${10 + 16} years old. I am from ${Batch}`

	console.log(fullName);
	console.log(fullName2);


// Q: Can we re-assign a new value to a variable? - Y
	let year2022 = "Tom";
	console.log(year2022)

	year2022 = "Aaron";
	console.log(year2022)

//  Q: Can we re-declare a variable? -No, we can only re-assign a new value but not re-declare same variable.

	// let job = 'instructor';

	// let job = 'software engineer';

// Why is variable important?
let brand = `ROG`;
brand = `Predator`;

console.log(brand);	//line 100
console.log(brand);	//line 200
console.log(brand);	//line 300
console.log(brand);	//line 400
console.log(brand);	//line 500
console.log(brand);	//line 600
console.log(brand);	//line 700
console.log(brand);	//line 800
console.log(brand);	//line 900
console.log(brand);	//line 1000

// Why is declaring a variable still important if variable would still work even without using let keyword?
myname = `christiana`;
console.log(myname);

	// variable can still be a container even without a present value
		let container;
		console.log(container);		//undefined

		//what if you want a new variable but without declaring/initializing a value
			//address;					//not defined
			//console.log(address)

/* Constant 
		- a type of variable that holds data/vbalue
		- using const keyword
		- we cannot re-assign a new value to a constant variable
*/
	
	const PI = 3.14;
	console.log(PI);

	const SECONDS = 60;
	const MINUTES = 60;
	const HOURS = 24;

	const sentence = `One minute has ${SECONDS} seconds, one hour has ${MINUTES} minutes, and one day has ${HOURS} hours.`


	//SECONDS = 60 * 2;	//error because re-assigning a new value to a constant variable is not accepted by JS

//Will declaring a constant variable without value works? -No, it should always come with a value because the concept must be constant and re-assignment should not be applied to constant variables.
	//example of declaring a constant variable w/o a value
		//const container2;	//error

	//example of re-assignment of value to a constant variable
		//container2 = "adobo"	//error


/*	Data Types
		- away to sort out data/values according to their type
	
	// primitive data types:

	1. String
		- sequence of characters
		- always wrapped around with backticks or quotation marks
		- if no quotation or backticks, JS will confuse the value with variable

	Examples:

		let flower = `Rose`;
		let mobile = `+639123456789`;

	2. Number
		- whole numbers (integer) & decimals (float)

	Example: 

		let num = 639123456789;
	
	3. Boolean
		- logical type that can only be true or false
		- use for taking decisions
		- with the help of operators, we can come up a value of true or false based on the evaluation

	Example:

		let isEarly = true;
		console.log(isEarly)

		let areLate = false;
		console.log(areLate);


	4. Undefined
		- value not yet assigned, or it could be empty value.

	Example:

		let data;
		console.log(data);			//undefined


		newData;
		console.log(newData)		//not defined



	5. Null
		- also means empty value
		- it is an assigned value to a variable that is used to represent an empty value

	let spouse = null;

	
	//reference data types

	6. Object
		- it has curly braces, properties & values
		- a single variable can hold multiple different types of data
*/
	let person = {
		// property: value
		name: "Tim",
		age: 29,
		address: {
			city: "Cainta",
			province: "Rizal"
		},
		isSoftwareEngineer: true,
		email: ["tim@mail.com", "therera@mail.com"],
		spouse: null
	}

	// Special type of object
	/*	Array
			- it has square brackets, and using index as a reference to each element inside the bracket
			- a single variable can contain collection of related data/values
	*/
	let grades = [91, 93, 95, 98];
	
	//This example is correct by syntax but not recommended since elements in an array should be relative
		// let random = ["bag", 100, true, undefined, null];

	/*  typeof() 
			- operator helps us determine what type of data are we working with
			- returns a string value
	*/

	console.log( "mercedes");				// mercede
	console.log( typeof("mercedes") );		// string

	console.log( 3.14 );					// 3.14
	console.log( typeof(3.14) );			// number


	console.log( true );					// true
	console.log( typeof(true) );			// boolean



/* Functions 
		- reusable pieces of code


	create hello world and display in the console 10x in hour

	create hello world and display in the console 100x in hour

	create hello world and display in the console 1000x in hour
*/
	
	function sayHello(){ //codeblock
		// statement
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
		console.log("Hello World");
	}
	// sayHello();
	// sayHello();
	// sayHello();

/* Function has two parts 

	Syntax:

		function <fname>(){
	
		}
		
		<fname>()

	1. function declaration
		- function keyword
		- function name
		- () = parameters
		- code block


	2. function invocation
		- function name
		- () = arguments
*/

// Simple function

	function intro(name, job){

		// console.log(`My name is Joy. I am a Full Stack Web Developer`);

		console.log(`My name is ${name}. I am a ${job}`);
	}
	intro("Joy", "Full Stack Developer");
	intro("Joseph", "Frontend Developer");
	intro("Dexter", "Software Engineer");
	intro("Earl", "Full Stack Web Dev Instructor");

/* Mini Activity

	Create a function that will accept 3 numbers and display the total sum of these 3 numbers in the console.

	Send the screenshot of your codes in group chat

*/
	function totalSum(num1, num2, num3){
		console.log(`${num1} plus ${num2} equals to ${num1 + num2} plus ${num3} equals to ${(num1 + num2) + num3}`)
	}
	totalSum(15, 38, 24);


// Return keyword

	function product(a, b){
		
		// console.log(a * b)
		return a * b
	}

	let result = product(2, 4);
	console.log(result);
	// console.log(product(2, 4))

